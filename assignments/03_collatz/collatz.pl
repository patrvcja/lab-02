% Zaimplementuj predykat collatz_iterations, która zwróci minimalną ilość iteracji równania
% z problemu Collatza potrzebnych aby otrzymać liczbę 1.
% Zobacz: https://pl.wikipedia.org/wiki/Problem_Collatza

collatz_iterations(X, Iterations) :- fail.



% Prymitywne unit testy,
% zapytanie 'test' powinno przetestować silnię
test :-
    foreach(test_case(I,E), test_result(I,E)), !.

test_case(-5, fail).
test_case(0, fail).
test_case(1, 0).
test_case(4, 2).
test_case(17, 12).
test_case(27, 111).

test_result(Input, ExpectedResult) :-
    ExpectedResult \= fail,
    \+ collatz_iterations(Input, _),
    format('ERROR: collatz_iterations(~w) -> expected ~w, got failure.\n', [Input, ExpectedResult]).
test_result(Input, ExpectedResult) :-
    ExpectedResult \= fail,
    collatz_iterations(Input, Result),
    \+ number(Result),
    format('ERROR: collatz_iterations(~w) -> expected ~w, did not get any number.\n', [Input, ExpectedResult]).
test_result(Input, ExpectedResult) :-
    ExpectedResult \= fail,
    collatz_iterations(Input, Result),
    Result \= ExpectedResult,
    format('ERROR: collatz_iterations(~w) -> expected ~w, got ~w.\n', [Input, ExpectedResult, Result]).
test_result(Input, ExpectedResult) :-   
    ExpectedResult \= fail,
    collatz_iterations(Input, ExpectedResult).
test_result(Input, fail) :-
    collatz_iterations(Input, Result),
    format('ERROR: collatz_iterations(~w) -> expected failure, got ~w.\n', [Input, Result]).
test_result(Input, fail) :-
    \+ collatz_iterations(Input, _).

