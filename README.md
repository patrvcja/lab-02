# Lab 02. Efekty uboczne i obliczenia

Celem tego laboratorium jest zapoznanie się z elementami Prologa wykraczającymi poza zwykłą rezolucję i logiczne operacje.

## Notatniki

- [efekty uboczne](https://swish.swi-prolog.org/?code=https://gitlab.com/agh-courses/23/loginf/lab-02/-/raw/master/notebooks/03_efekty_uboczne.swinb)
- [arytmetyka](https://swish.swi-prolog.org/?code=https://gitlab.com/agh-courses/23/loginf/lab-02/-/raw/master/notebooks/04_obliczenia.swinb)

## Zadania

Proszę uzupełnić odpowiednie pliki w katalogu `assignments`.
